#include <cassert>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "matriz.h"

using namespace std;

Matriz::Celda::Celda()
: centroide(0.0), id(nullptr), miForma(0)
{
}

Matriz::Celda::Celda(const Celda& otra)
: centroide(otra.centroide), id(strdup(otra.id)), miForma(otra.miForma)
{
}

Matriz::Celda::Celda(double cen, char* id)
: centroide(cen), id(id), miForma(0)
{
}

Matriz::Celda::~Celda()
{
}

Matriz::Matriz(size_t filas)
: filas(filas), columnas(MAX_FORMAS)
{
	/*Se inicializa con celdas cuyo centroide es infinito, para reemplazarlas luego por valores reales.*/
	matriz = new Celda ** [filas];
	for(size_t i = 0; i < filas; ++i){
		matriz[i] = new Celda * [MAX_FORMAS];
		for(size_t j = 0; j < columnas; ++j)
			matriz[i][j] = new Celda(INFINITO, nullptr);
	}
}

Matriz::~Matriz()
{
	if(!matriz)
		return;
	for(size_t i = 0; i < filas; ++i){
		for(size_t j = 0; j < columnas; ++j){
			delete matriz[i][j];
		}
	}
	for(size_t i = 0; i < filas; ++i)
		delete [] matriz[i];
	delete [] matriz;
}

void Matriz::insertar(acorde_t* acorde, size_t fila)
{
	for(size_t i = 0; i < columnas; i++){
		if(acorde->centroides[i] == 0.0){
			matriz[fila][i]->centroide = INFINITO;
		}else{
			matriz[fila][i]->centroide = acorde->centroides[i];
		}
		matriz[fila][i]->id = &acorde->id[0];
		matriz[fila][i]->miForma = (int)i+1;
	}
}

double Matriz::distancia(double c1, double c2)
{
	return fabs(c2-c1);
}

void Matriz::imprimir()
{
	for(size_t i = 0; i < this->filas; ++i){
		for(size_t j = 0; j < this->columnas; ++j){
			if(matriz[i][j])
				cout << "( " << (this->matriz[i][j])->id << ", " << (this->matriz[i][j])->centroide << " ) ";
		}
		cout << endl;
	}
}

double Matriz::Fase()
{
	//matriz oraculo
	double** f = new double*[filas]();
	int** sigma = new int*[filas]();
	for(size_t i = 0; i < filas; ++i){
		f[i] = new double[columnas]();
		sigma[i] = new int[columnas]();
	}
		
	double d1 = 0;
	double d2 = 0;
	double d3 = 0;
	double menor = 0.0;
	for(int i = filas-1; i >= 0; --i){
		for(size_t j = 0; j < columnas; ++j){
			if(i >= (int)filas-1 || matriz[i][j]->centroide == INFINITO)
				continue;
			d1 = distancia(matriz[i][j]->centroide, matriz[i+1][0]->centroide) + f[i+1][0];
			d2 = distancia(matriz[i][j]->centroide, matriz[i+1][1]->centroide) + f[i+1][1];
			d3 = distancia(matriz[i][j]->centroide, matriz[i+1][2]->centroide) + f[i+1][2];
			menor = (d1 < d2 ? (d1 < d3 ? d1 : (d2 < d3 ? d2 : d3)) : (d2 < d3 ? d2 : d3));
			f[i][j] = menor;
			if(d1 == menor)
				sigma[i][j] = 0;
				else
					if(d2 == menor)
						sigma[i][j] = 1;
						else
							sigma[i][j] = 2;
		}
	}
	d1 = f[0][0];
	d2 = f[0][1];
	d3 = f[0][2];
	double distMenor = (d1 < d2 ? (d1 < d3 ? d1 : (d2 < d3 ? d2 : d3)) : (d2 < d3 ? d2 : d3) );
	int indiceMenor = 0;
	if(distMenor == d1)
		indiceMenor = 0;
		else
			if(distMenor == d2)
				indiceMenor = 1;
				else
					indiceMenor = 2;
	size_t j = indiceMenor;
	for(size_t i = 0; i < this->filas; ++i){
		cout << setw(5) << left << matriz[i][0]->id << " -> forma: " << j+1 << endl;
		j = sigma[i][j];
	}
	for(int i = 0; i < (int)filas; ++i){
		delete [] f[i];
		delete [] sigma[i];
	}
	delete [] f;
	delete [] sigma;
	return distMenor;
}

void Matriz::obtenerSolucion()
{
	double distMenor = Fase();
	printf("Distancia: %.2lf\n", distMenor);
	double distPromedio = distMenor/(double)filas;
	printf("Distancia promedio: %.2lf\n", distPromedio);
}
