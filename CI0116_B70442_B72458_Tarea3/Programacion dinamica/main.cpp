#include <iostream>
#include <time.h>
#include <stdio.h>
#include "lector.h"

using namespace std;


/**Función recursiva que estima el tiempo de ejecución del algoritmo para una canción de x acordes.*/
unsigned long long func(unsigned long long x)
{
	if(x <= 15)
		return 5;
	else
		return 3*func(x-1);
}

int main(int argc, char * argv[])
{
    clock_t start, end;
    double secs;
	(void) argc;
	Lector lector;
	if(argc < 3){
		cerr << "Error! faltan argumentos!";
		return 1;
	}
    lector.leerCSV(argv[1]);
    Matriz* matriz = lector.cargarMatriz(argv[2]);
    size_t acordes = matriz->getFilas();
    cout << "Cantidad de acordes: " << acordes << endl;
    start = clock();
    matriz -> obtenerSolucion();
    end = clock();
    secs = (double)(end-start) / (double)CLOCKS_PER_SEC;
    printf("Tiempo: %.6lfs\n", secs);
    delete matriz;
    return 0;
}
