#include <cassert>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "matriz.h"

using namespace std;

Matriz::Celda::Celda()
: centroide(0.0), id(nullptr), miForma(0)
{
}

Matriz::Celda::Celda(const Celda& otra)
: centroide(otra.centroide), id(strdup(otra.id)), miForma(otra.miForma)
{
}

Matriz::Celda::Celda(double cen, char* id)
: centroide(cen), id(id), miForma(0)
{
}

Matriz::Celda::~Celda()
{
}

Matriz::Matriz(size_t filas)
: filas(filas), columnas(MAX_FORMAS)
{
	/*Se inicializa con celdas cuyo centroide es infinito, para reemplazarlas luego por valores reales.*/
	matriz = new Celda ** [filas];
	for(size_t i = 0; i < filas; ++i){
		matriz[i] = new Celda * [MAX_FORMAS];
		for(size_t j = 0; j < columnas; ++j)
			matriz[i][j] = new Celda(INFINITO, nullptr);
	}
}

Matriz::~Matriz()
{
	if(!matriz)
		return;
	for(size_t i = 0; i < filas; ++i){
		for(size_t j = 0; j < columnas; ++j){
			delete matriz[i][j];
		}
	}
	for(size_t i = 0; i < filas; ++i)
		delete [] matriz[i];
	delete [] matriz;
}

void Matriz::insertar(acorde_t* acorde, size_t fila)
{
	for(size_t i = 0; i < columnas; i++){
		matriz[fila][i]->centroide = acorde->centroides[i];
		matriz[fila][i]->id = &acorde->id[0];
		matriz[fila][i]->miForma = (int)i+1;
	}
}

double Matriz::distancia(const vector<Matriz::Celda *>& camino)
{
	double acumulado = 0;
	for(size_t i = 1; i < camino.size(); ++i){
		acumulado += fabs(camino[i-1]->centroide - camino[i]->centroide);
	}
	return acumulado;
}

void Matriz::imprimir()
{
	for(size_t i = 0; i < this->filas; ++i){
		for(size_t j = 0; j < this->columnas; ++j){
			if(matriz[i][j])
				cout << "( " << (this->matriz[i][j])->id << ", " << (this->matriz[i][j])->centroide << " ) ";
		}
		cout << endl;
	}
}

vector<Matriz::Celda *>* Matriz::minimo(vector<Matriz::Celda*>& c, vector<Matriz::Celda*>& cp, vector<Matriz::Celda*>& cpp)
{
	vector<Matriz::Celda *>* menor;
	if(this->distancia(c) < this->distancia(cp) && this->distancia(c) < this->distancia(cpp) ){
		menor = &c;
	}else{
		if(this->distancia(cp) < this->distancia(cpp)){
			menor = &cp;
		}else{
			menor = &cpp;
		}
	}
	return menor;
}

vector<Matriz::Celda *> Matriz::Fase(size_t i, vector<Matriz::Celda*> camino)
{
	if(i >= this->filas)
		return camino;

	//vector principal c = camino, cp = camino', cpp = camino''
	vector<Matriz::Celda *> c = camino;
	vector<Matriz::Celda *> cp;
	vector<Matriz::Celda *> cpp;
	//cada uno toma diferentes centroides
	c[i] = this->matriz[i][0];
	cp = c;
	cp[i] = this->matriz[i][1];
	cpp = c; 
	cpp[i] = this->matriz[i][2];
	//se pregunta a la fase siguiente que pasa con cada eleccion de centroide
	//si la forma c[i] del acorde no existe, no hay nada que calcular
	if(c[i]->centroide != INFINITO)
		c = Fase(i+1, c);
	if(cp[i]->centroide != INFINITO)
		cp = Fase(i+1, cp);
	if(cpp[i]->centroide != INFINITO)	
		cpp = Fase(i+1, cpp);
	//se devuelve el camino que tenga la distancia menor en la i-ésima fase
	return *(minimo(c, cp, cpp));
}

void Matriz::obtenerSolucion()
{
	this->solucion.resize(this->filas);
	vector<Matriz::Celda *> solucion = this->Fase(0, this->solucion);
	for(size_t i = 0; i < solucion.size(); ++i){
		cout << setw(3) << left << solucion[i]->id << " -> forma: " << solucion[i]->miForma << endl;
	}
	double distanciaTotal = this->distancia(solucion);
	printf("Distancia: %.2lf\n", distanciaTotal);
	cout << "Distancia promedio entre acordes: " << (distanciaTotal / (double)this->filas) << endl;
}
