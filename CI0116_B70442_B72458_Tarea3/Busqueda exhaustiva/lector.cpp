#include <cassert>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>

#include "lector.h"
#include "matriz.h"

using namespace std;

Lector::Lector()
{

}

void Lector::leerCSV(char * archivo){
	acorde_t acordeActual;
	ifstream data(archivo);
	if(!data){
		cerr << "Error! No se encontro el archivo .csv" << endl;
		exit(1);
	}
	array<int, COLUMNAS> arreglo{-1,-1,-1,-1,-1,-1,-1}; //inicializacion por defecto con valores centinela
	int index = 0;
	int formas = 0;
	int centroideActual = 0;
	string line;
	//lee el archivo por filas
	while(getline(data, line)){
		stringstream lineStream(line);
		string campo;
		getline(lineStream,campo,','); //lee el nombre del acorde
		strcpy(&acordeActual.id[0], campo.c_str()); //lo copia en el acorde actual
		//procesa cada columna de la fila actual
		while(getline(lineStream, campo, ',')){
			if(atoi(campo.c_str())){
				arreglo[index] = atoi(campo.c_str()); 
			}else{
				if(!strcmp(campo.c_str(), "0")){
					arreglo[index] = 0;
				}else{
					arreglo[index] = -1; //valor centinela que indica que la cuerda no se debe tocar ni pulsar
				}
			}
			//Si se termino de leer una forma del acorde, procesela y agreguela al vector de centroides
			if(index == COLUMNAS-1){
				acordeActual.centroides[centroideActual] = calcularCentroide(arreglo);
				if(acordeActual.centroides[centroideActual] > 0.0)
					++formas;
				++centroideActual;	
				reiniciarArreglo(arreglo);
				index = -1;
			}
			++index;
		}
			//Si se termino de procesar toda una fila, entonces meta el acorde a la lista y siga con la siguiente fila
		acordeActual.formas = formas;
		lista.push_back(acordeActual);
		reiniciarAcorde(acordeActual);	
		formas = 0;
		index = 0;
		centroideActual = 0;
	}
	data.close();
}

double Lector::calcularCentroide(array<int, COLUMNAS>& arreglo){
	int validos = 0;	//validos son aquellos distintos de -1
	int sumaTrastes = 0;
	int desplazamiento = 0;
	if(arreglo[0] != -1){
		desplazamiento = arreglo[0] - 1;
	}
	//empieza en 1 porque ya se proceso el primer campo: el desplazamiento
	for(int index = 1; index < COLUMNAS; ++index){
		if(arreglo[index] != -1){
			sumaTrastes += arreglo[index] + desplazamiento; //si el valor es valido, agreguelo a la suma
			++validos;
		}
	}
	return validos > 0? (double)sumaTrastes / (double)validos : 0.0; //devuelve el promedio del arreglo de trastes: el centroide
}

void Lector::reiniciarArreglo(array<int, COLUMNAS>& arreglo){
	for(int index = 0; index < COLUMNAS; ++index){
		arreglo[index] = -1;
	}
}

void Lector::reiniciarAcorde(acorde_t& acordeActual){
	strcpy(&acordeActual.id[0], "\0");
	for(int index = 0; index < MAX_FORMAS; ++index){
		acordeActual.centroides[index] = 0.0;
	}
}

Matriz* Lector::cargarMatriz(char *ruta)
{
	Matriz* matriz = new Matriz(contarLineas(ruta));
	fstream archivo(ruta, ios::in);
	string lineaActual;
	char* acorde;
	acorde_t* ac;
	size_t filaActual = 0;
	if(!archivo){
		cerr << "Error! No se encontro el archivo .txt" << endl;
		exit(1);
	}
	int linea = 0;
	while(getline(archivo, lineaActual)){
		++linea;
		if((int)lineaActual.find('/') != -1)
			lineaActual = lineaActual.substr(0, lineaActual.rfind('/'));
		acorde = const_cast<char *>(lineaActual.c_str());
		assert(acorde);
		ac = this->buscarEnLista(acorde, this->lista);
		if(ac){ //si encontró el acorde, insértelo a la matriz
			matriz->insertar(ac, filaActual++);
		}else{
			cerr << "El acorde " << acorde << " en línea " << linea << " no se encuentra en el .csv!" << endl;
			exit(1);
		}
	}
	archivo.close();
	return matriz;
}

acorde_t* Lector::buscarEnLista(char *id, list<acorde_t>& lista)
{
	if(lista.empty())
		return nullptr;
	for(auto iterador = lista.begin(); iterador != lista.end(); ++iterador){
		if(strcmp((const char *)id, (const char *)(*iterador).id) == 0){
			return &(*iterador);
		}
	}
	return nullptr;
}

size_t Lector::contarLineas(char* ruta)
{
	fstream archivo(ruta, ios::in);
	if(!archivo){
		cerr << "Error! No se encontro el archivo .txt" << endl;
		exit(1);
	}
	string innecesaria; //se necesita solo para cargar el archivo linea por linea y contar las lines
	size_t lineas = 0;
	while(getline(archivo, innecesaria))
		++lineas;
	archivo.close();
	return lineas;
}
