#ifndef MATRIZ_H
#define MATRIZ_H
#define NOMBRE_MAX 10
#define MAX_FORMAS 3
#include <array>
#include <limits>
#include <vector>

using namespace std;

/**Estructura para agrupar los elementos de un acorde: su nombre, sus centroides y las formas de interpretarlo.*/
typedef struct
{
   char id[NOMBRE_MAX];
   array<double, MAX_FORMAS> centroides;
   size_t formas;
} acorde_t;

/**@class Matriz
 * Clase diseñada para proporcionar una matriz que almacene todos los acordes de una canción por filas, para luego
 * recorrerla recursivamente de manera que se realicen todos los recorridos posibles para determinar la combinación
 * que minimiza la distancia total entre los acordes de la canción.
*/
class Matriz
{
	public:
	/**@class Celda
	 * La Matriz se llena de objetos de esta clase, que cuenta con lo necesario para determinar en varios recorridos 
	 * la distancia menor entre los acordes de la canción.
	 */
		class Celda{
			friend class Matriz;
			
			private:
				double centroide;
				char* id; //nombre del acorde
				int miForma; //centroide de ese acorde: 1, 2 o 3
				
			public:
				Celda();
				Celda(const Celda &);
				Celda(double, char*);
				~Celda();
		};
	
		/**Constructor que crea una matriz de celdas de tamaño filas * 3, inicialmente con celdas cuyo centroide
		 * es infinito, para que no afecten la búsqueda del menor cuando un acorde tenga menos de 3 formas de 
		 * ser interpretado.
		 */
		Matriz(size_t filas = 0);
		/**Destructor.
		 */
		~Matriz();
		/**Función para insertar un acorde a la matriz. Ésta reemplaza los datos de la celda en la fila indicada 
		 * con los datos del acorde a ser insertado.
		 * @param acorde Acorde a ser insertado.
		 * @param fila Fila donde se desea insertar el acorde.
		 * */
		void insertar(acorde_t *acorde, size_t fila);
		/**Método utilitario para efectos de depuración.
		 */
		void imprimir();
		/**Función que se encarga de obtener e imprimir el vector sigma, cuyas entradas representan la forma ideal 
		 * de interpretar cada acorde de manera tal que la distancia total de la pieza es la menor.
	     */
		void obtenerSolucion();
		
		/**Función utilitaria para obtener el número de filas de la matriz, es decir, la cantidad de acordes de la canción.
		 * @return Cantidad de acordes de la canción.
		 */
		size_t getFilas()
		{
			return this->filas;
		}
		
	private:
		size_t filas;
		size_t columnas;
		const double INFINITO = std::numeric_limits<double>::infinity();
		/**Matriz de punteros a Celda.*/
		Celda*** matriz;
		/**Método para calcular la distancia entre una determinada secuencia de centroides.*/
		double distancia(const vector<Celda *>& camino);
		/**Vector sigma en el que se da la solución al problema.*/
		vector<Celda *> solucion;
		/**Método recursivo principal que se encarga de explorar todas las posibles combinaciones entre los centroides 
		 * de los acordes, y devuelve el vector solución.
		 * @param i Parámetro para llevar la cuenta de la fase actual.
		 * @param camino Vector que contiene el camino o secuencia de acordes para procesar en la i-ésima fase.
		 * @return un vector de punteros a Celda con la solución óptima al problema.
		 */
		vector<Celda *> Fase(size_t i, vector<Celda*> camino);
		/**Método que encuentra la secuencia de menor distancia entre tres secuencias de centroides.
		 * @param c, cp, cpp Vectores de punteros a Celda que se compararán entre sí.
		 * @return un puntero al vector de menor distancia que ingresó como argumento.
		 * */
		vector<Celda *>* minimo(vector<Celda*>& c, vector<Celda*>& cp, vector<Celda*>& cpp);
};


#endif
