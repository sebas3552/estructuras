#ifndef LECTOR_H
#define LECTOR_H
#define COLUMNAS 7
#include <array>
#include <list>

#include "matriz.h"

using namespace std;

/**@class Lector
 * Clase encargada de realizar la gestión de archivos de entrada: .csv y .txt.
 * También se encarga de rellenar la Matriz de punteros a Celda con los acordes leídos de la canción en formato .txt,
 * así como de almacenar en una lista enlazada todos los acordes leídos del archivo .csv.
 */
class Lector
{
	private:
		/**Lista que almacena todo el diccionario de acordes del archivo .csv proporcionado.*/
		list<acorde_t> lista;
		/**Método que, dado un arreglo que simboliza los trastes de una forma de un acorde, calcula y devuelve su centroide.*/
		double calcularCentroide(array<int, COLUMNAS> &arreglo);
		/**Método utilitario para cargar la lista de acordes.*/
		void reiniciarArreglo(array<int, COLUMNAS> & arreglo);
		/**Método utilitario para cargar la lista de acordes.*/
		void reiniciarAcorde(acorde_t & acordeActual);
		/**Método para buscar un acorde específico en la lista de acordes, imprime un mensaje de error si no se encuentra.*/
		acorde_t* buscarEnLista(char *, list<acorde_t>& lista);
		/**Método para contar las líneas del archivo .txt que contiene los acordes de una canción dada.*/
		size_t contarLineas(char* ruta);
	
	public:
		Lector();
		/**Método para leer el archivo .csv y cargar la lista con esa información.
		 * @param archivo Nombre o ruta del archivo .csv.
		 */
		void leerCSV(char *archivo);
		/**Método para cargar la matriz de acordes con los que contenga la canción, en formato .txt.
		 * @param archivo Nombre o ruta del archivo .txt que contiene los acordes de una canción dada.
		 * @return Puntero a una Matriz creada en memoria dinámica con la información de los acordes de la canción;
		 * produce un error si un acorde de la canción no se encuentra en la lista de acordes cargados del .csv.
		 */
		Matriz* cargarMatriz(char *archivo);
};

#endif // LECTOR_H
