%% LyX 2.1.4 created this file.  For more info, see http://www.lyx.org/.
%% Do not edit unless you really know what you are doing.
\documentclass[twocolumn,english,spanish,journal]{IEEEtran}
\usepackage{newtxmath}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\synctex=-1
\usepackage[spanish]{babel}
\addto\shorthandsspanish{\spanishdeactivate{~<>}}
\usepackage{float}
\usepackage{booktabs}
\usepackage{siunitx}
\usepackage{calc}
%\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage[table,xcdraw]{xcolor}
%\usepackage{algorithm}
\usepackage[spanish]{babel}
\selectlanguage{spanish}
\usepackage[spanish,onelanguage]{algorithm2e}
\usepackage[unicode=true,pdfusetitle,
 bookmarks=true,bookmarksnumbered=false,bookmarksopen=false,
 breaklinks=false,pdfborder={0 0 1},backref=false,colorlinks=false]
 {hyperref}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% LyX specific LaTeX commands.
%% Because html converters don't know tabularnewline
\providecommand{\tabularnewline}{\\}
\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algoritmo}
\floatname{algorithm}{\protect\algorithmname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
 % protect \markboth against an old bug reintroduced in babel >= 3.8g
 \let\oldforeign@language\foreign@language
 \DeclareRobustCommand{\foreign@language}[1]{%
   \lowercase{\oldforeign@language{#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{graphicx}
\usepackage{pgfplots}
\usepgfplotslibrary{groupplots}
\pgfkeys{/pgf/number format/.cd, set thousands separator=\,}

\makeatother

\usepackage{listings}
\addto\captionsenglish{\renewcommand{\algorithmname}{Algorithm}}
\addto\captionsenglish{\renewcommand{\lstlistingname}{Listing}}
\addto\captionsspanish{\renewcommand{\algorithmname}{Algoritmo}}
\addto\captionsspanish{\renewcommand{\lstlistingname}{Listado de código}}
\renewcommand{\lstlistingname}{Listado de código}


\begin{document}

\title{Minimización del desplazamiento total entre acordes en canciones interpretadas en guitarra}


\author{Katherine Angulo Mendoza - B70442 y Sebastián Cruz Chavarría - B72458}

\selectlanguage{spanish}

\markboth{Estructuras de Datos y Análisis de Algoritmos -- Tarea III -- Katherine Angulo Mendoza y Sebastián Cruz Chavarría}{}
\maketitle
\selectlanguage{spanish}
\begin{abstract}
En este trabajo se resolvió un problema de optimización a través de dos enfoques particulares: búsqueda exhaustiva y programación dinámica. Se definió el espacio donde reside la solución, así como los algoritmos que encuentran la solución óptima dado un conjunto de datos de entrada. Se probó el programa variando el tamaño de la entrada y se tomaron los tiempos de ejecución para determinar cuál método es más eficiente. Los resultados fueron lo suficientemente contundentes para afirmar que solo se debe utilizar la búsqueda exhaustiva como paso intermedio hacia otras soluciones más eficientes, como la programación dinámica.
\end{abstract}


\section{Introducción}

\IEEEPARstart{P}{ara} interpretar una canción en guitarra existen varias formas de representar cada acorde, de manera que existe una gran cantidad de combinaciones entre éstos, cuya distancia total es variable. El problema que pretende resolver este trabajo es el de \emph{encontrar la mejor combinación entre las formas de reproducir cada acorde, de manera tal que el desplazamiento total entre ellos sea el menor}, es decir, que se reduzca al mínimo posible el movimiento total de la mano izquierda. Existen diversas formas de resolver este problema, entre las cuales, para efectos de este trabajo, se seleccionaron dos: búsqueda exhaustiva y programación dinámica. Como objetivos secundarios se incorporaron los siguientes: primero, comprobar que la solución obtenida efectivamente es la óptima; y segundo, decidir cuál es el enfoque más eficiente entre los mencionados anteriormente. 


\section{Metodología}

Básicamente, el programa debe recibir como entrada los acordes de una canción, separados por cambios de línea, y un archivo separado por comas (.csv) en el que se listen todos los posibles acordes musicales de guitarra, seguidos de las distintas formas de representar cada uno. Cada fila del archivo puede contener hasta tres formas de interpretar ese acorde, en un grupo de números que siguen el siguiente formato: el primer dígito indica la posición del primer traste del diagrama, seguido de seis dígitos en el intervalo $[1, 5]$ en donde cada uno indica el número de traste en el que debe presionarse cada cuerda, relativo al desplazamiento del primer traste del diagrama. Como salida el programa dará, para cada acorde en la canción, el nombre del acorde seguido de un número en el intervalo $[1, 3]$ que indica la forma óptima de interpretar ese acorde, de manera tal que la suma de todos los desplazamientos entre los centroides es la menor.

Todas las posibles combinaciones de acordes en las que se puede interpretar la canción se representaron mediante un \emph{árbol ternario}, es decir, un árbol en el que cada nodo tiene tres hijos como máximo. Cada nodo representa una forma de interpretar un acorde específico, de manera tal que contiene el valor de su centroide y tres punteros a sus hijos, como máximo. Dado que la cantidad de nodos en un árbol ternario crece exponencialmente en el orden de $3^n$, el espacio que éste necesita limita la cantidad de acordes con los que se puede trabajar. Por tanto, para efectos de la implementación, se utilizó una matriz de $n$ filas por $3$ columnas, donde cada fila representa un acorde y cada columna una forma de interpretarlo. De esta manera, se reduce el espacio necesario por el programa. El programa se implementó en C++. Para el almacenamiento de los acordes se utilizó la clase \texttt{list} de la STL.

\subsection{Búsqueda exhaustiva}

Formalmente, la solución del problema está dada por un vector $\vec{\sigma} = <\sigma_1, \sigma_2, ..., \sigma_n>$, donde cada elemento $\sigma_i \in \mathbb{N}_3$ representa la forma óptima de interpretar el $i$-ésimo acorde (para $i=1,2,...,n$), y $n$ es la cantidad de acordes de la canción proporcionada en la entrada. El espacio al que pertenece la solución óptima está denotado por $E = \mathbb{N}_3^n$, y su cardinalidad es $|E|= 3^n$. Dado que todos los acordes no tienen exactamente tres formas de intepretarse, el espacio acotado $E'$ corresponde a todas las combinaciones entre las formas \emph{disponibles} de interpretar cada uno. La cardinalidad del espacio acotado es entonces $|E'| \leq 3^n$. El algoritmo que explora todas las soluciones candidatas en el espacio acotado y devuelve la óptima, en términos de búsqueda exhaustiva, se describe en el algoritmo \ref{alg:saludo-tico}.

Las pruebas del programa se llevaron a cabo en una computadora con procesador Intel G630 a $2.8$ GHz y 4 Gb de memoria RAM. Dado que el tiempo de ejecución para algunas canciones es excesivo, se tomó una aproximación del tiempo de ejecución en segundos para $n=15$ (cantidad de acordes) y se aproximó el tiempo que tomaría el algoritmo (en segundos) para ejecutarse con canciones de más de $15$ acordes, mediante la siguiente función recursiva:

\begin{equation}
\notag
f(n)= \left\{ \begin{array}{lc}
             5, & \text{si } n \leq 15 \\
             \\ 3 f(n-1), & \text{en otro caso}. \\
             \end{array}
   \right. 
\end{equation}

La ejecución del algoritmo para un cierto $n$ tarda aproximadamente \emph{tres veces} lo que tarda la ejecución para $n-1$ dado que el espacio que debe recorrer el algoritmo para encontrar la solución óptima es del orden de $\Theta(3^n)$ \cite{Cormen:2009}. De esta manera, la aproximación de los tiempos de ejecución mediante la función descrita anteriormente permitió estimar los resultados para las canciones en las que $n$ es grande.

\subsection{Programación dinámica}

En cuanto al método de programación dinámica, el vector solución $\sigma$, el espacio de la solución y su cardinalidad son los mismos que en el método de búsqueda exhaustiva. Sin embargo, para obtener la solución óptima de una manera más eficiente, se define el oráculo $f_i [j]$ como la distancia mínima acumulada desde el acorde $i$ hasta $n$, donde el acorde $i$ se representa en su $j$-ésima forma, para $i = 1,2,...,n$ y $j=1,2,3$. De esta manera, el valor objetivo a obtener a partir del oráculo es \emph{mín}$\{f_1[1], f_1[2], f_1[3]\}$, que representa la distancia mínima total entre todos los acordes de la canción.

Paso base:

\begin{equation}
\notag
f_n[j]=0,~(j=1,2,3).
\end{equation}

Paso recursivo:

\begin{equation}
\notag
f_i[j]= min\left\{ \begin{array}{l}
              \text{distancia}(m[i,j], m[i+1,0])+f_{i+1}[0], \\
             \\ \text{distancia}(m[i,j], m[i+1,1])+f_{i+1}[1],\\
             \\ \text{distancia}(m[i,j], m[i+1,2])+f_{i+1}[2]\\
             \end{array}
 \right.
\end{equation}

El código que encuentra la solución óptima mediante programación dinámica se muestra en el algoritmo \ref{alg:dinamica}. El tiempo de ejecución mediante esta técnica se redujo de $\Theta(3^n)$ a $\Theta(n)$, es decir, se pasó de un tiempo exponencial en base $3$ a tiempo lineal, lo cual indica que la programación dinámica es más eficiente que el enfoque anterior de búsqueda exhaustiva. Esto porque el método dinámico \emph{guarda} los resultados a medida que los calcula, mientras que el método exhaustivo los \emph{recalcula} en cada iteración del algoritmo. Los tiempos de ejecución para los casos de prueba seleccionados se muestran en el cuadro \ref{tab:tiempos}, y la comparación asintótica se muestra en la figura \ref{fig:log}.

\subsection{Algoritmos ávidos}

Una solución ávida generalmente consiste en elegir lo que parezca mejor localmente \cite{Cormen:2009}. Para este problema, un enfoque ávido consiste en elegir el centroide más pequeño para cada acorde, pero esta elección no necesariamente lleva a la solución óptima. Esto se demuestra con mayor claridad en el siguiente ejemplo. Suponga que se tiene la siguiente tabla de centroides:
\begin{table}[h!]
\centering
\begin{tabular}{lllll}
                        & 1                                                & 2                                                & 3                                                &  \\ \cline{2-4}
\multicolumn{1}{l|}{Am} & \multicolumn{1}{l|}{\cellcolor[HTML]{FFCB2F}2.0} & \multicolumn{1}{l|}{\cellcolor[HTML]{32CB00}5.9} & \multicolumn{1}{l|}{8.2}                         &  \\ \cline{2-4}
\multicolumn{1}{l|}{Bm} & \multicolumn{1}{l|}{\cellcolor[HTML]{FFCC67}1.4} & \multicolumn{1}{l|}{3.6}                         & \multicolumn{1}{l|}{\cellcolor[HTML]{32CB00}6.1} &  \\ \cline{2-4}
\multicolumn{1}{l|}{G}  & \multicolumn{1}{l|}{\cellcolor[HTML]{FFCC67}0.7} & \multicolumn{1}{l|}{4.1}                         & \multicolumn{1}{l|}{\cellcolor[HTML]{32CB00}6.2} &  \\ \cline{2-4}
\end{tabular}
\end{table}

El desplazamiento mínimo mediante un algoritmo ávido (selección amarilla) está dado por la siguiente expresión:

$|2.0-1.4| + |1.4-0.7|=1.3$

Ahora bien, la solución óptima (selección verde) está dada por la expresión

$|5.9-6.1|+|6.1-6.2|=0.3$

Evidentemente, los resultados obtenidos son distintos, por lo que se concluye que no es posible dar una solución ávida al problema.

\section{Resultados}

Los tiempos de ejecución de los casos de prueba seleccionados se muestran
en el cuadro~\ref{tab:tiempos}. Mediante el enfoque de búsqueda exhaustiva los resultados son explícitos en cuanto a la imposibilidad de medirlos realmente. Para las canciones con una cantidad moderada de acordes, las estimaciones arrojaron tiempos de ejecución en el orden de los miles de millones de años. La aproximación del tiempo de ejecución mediante búsqueda exhaustiva para la canción \emph{One} es de aproximadamente \emph{17 mil millones de años}, casi cuatro veces la edad de la Tierra. Los demás resultados en este apartado no difieren mucho del obtenido para la canción \emph{One}. De esta manera, los resultados apuntan a que la técnica de búsqueda exhaustiva es poco útil cuando se trata de problemas de mayor tamaño.
Por otra parte, los resultados obtenidos mediante programación dinámica son más asequibles, con tiempos menores a un segundo en todos los casos. La salida del programa mediante ambos enfoques se comparó contra casos de prueba preestablecidos y se confirmó que la solución obtenida fue la esperada (cuando fue posible obtenerla).

En la figura \ref{fig:log} se muestran los resultados obtenidos en ambos enfoques, en escala logarítmica. La curva correspondiente a búsqueda exhaustiva tiene un comportamiento exponencial, a medida que aumenta el tamaño de la entrada, mientras que la curva del método dinámico muestra una tendencia lineal. Ambos resultados concuerdan con la cota asintótica obtenida para cada método de $\Theta(3^n)$ y $\Theta(n)$ respectivamente.
\begin{table}
\caption{Tiempo de ejecución de los casos de prueba.\label{tab:tiempos}}


\centering{}%
\begin{tabular}{lrrr}
\toprule 

Método & Canción & Cant. de acordes & Tiempo (s) \tabularnewline
\midrule 
B.E & Círculo de \emph{Gm} & $8$ & $0.001$\tabularnewline
 & \emph{Heaven} & $54$ & \num{1.6e17}\tabularnewline
 & \emph{A Whole New World} & $110$ & \num{2.3e17} \tabularnewline
 & \emph{One} & $198$ & \num{5.4e17}\tabularnewline

\midrule
P.D. & Círculo de \emph{Gm} & $8$ & $0.000087$\tabularnewline
 & \emph{Heaven} & $54$ & $0.000382$\tabularnewline
 & \emph{A Whole New World} & $110$ & $0.000628$ \tabularnewline
 & \emph{One} & $198$ & $0.001177$\tabularnewline
%\midrule
%A.A. & Círculo de \emph{Gm} & $8$ & $0.001$\tabularnewline
% & \emph{Heaven} & $54$ & \num{1.6e17}\tabularnewline
% & \emph{A Whole New World} & $110$ & \num{2.3e17} \tabularnewline
% & \emph{One} & $198$ & \num{5.4e17}\tabularnewline
\bottomrule
\end{tabular}
\end{table}

\begin{figure*}
\begin{centering}
\begin{tikzpicture} 
    \begin{semilogyaxis}[
         width=\columnwidth,
         xtick={10,50,100,200},
         xlabel=Cantidad de acordes,
         ylabel=Tiempo (s),
         legend pos=outer north east]
    \addplot coordinates{(10,0.02) (54,295245) (100,2035243777248354431) (200,13566562784004001015)};
     \legend{Búsqueda exhaustiva, Programación dinámica};
    \addplot coordinates{(10,0.000087) (54,0.000382) (100,0.000628) (200,0.001177)}; 
   
    \end{semilogyaxis}
\end{tikzpicture}
\par\end{centering}
\caption{Comparación de los tiempos de ejecución para búsqueda exhaustiva y programación dinámica.\label{fig:log}}
\end{figure*}

\section{Conclusiones}

A partir de los resultados se concluye que la solución propuesta para el problema mediante el enfoque de programación dinámica es la más eficiente posible, a diferencia del enfoque de búsqueda exhaustiva que tiene poca capacidad de resolver el problema cuando el tamaño de la entrada es grande. No obstante, este enfoque es un paso importante hacia el diseño de la solución mediante el método dinámico, ya que permite construirla de una manera más sencilla. Por otra parte, la técnica de algoritmos ávidos no presenta una solución viable para este problema, por lo que no se implementó. Por lo tanto, la solución óptima para el presente problema se alcanza mediante la programación dinámica.


\appendices{}


\section{Código de los algoritmos}

Parte del código utilizado en este trabajo se muestra en los algoritmos \ref{alg:saludo-tico} y \ref{alg:dinamica}.
\begin{algorithm}

\caption{Algoritmo que encuentra la distancia mínima entre los acordes de una canción mediante búsqueda exhaustiva y devuelve el vector solución.\label{alg:saludo-tico}}
\SetAlgoLined

\SetKwFunction{KwFn}{Fase}
$c_{min} \leftarrow $\KwFn{$i$, $camino$}{

	\Indp\If{$i$ > $n$}{
		\Return{camino}\;
	}
	$c = camino$\;
	
	$c[i] = m[i][1]$\;
	
	$c' = c$\;
	
	$c'[i] = m[i][2]$\;
	
	$c'' = c$\;
	
	$c''[i] = m[i][3]$\;
	
	$c = $ \KwFn{$i+1$, $c$}\;
	
	$c'= $ \KwFn{$i+1$, $c'$}\;
	
	$c'' = $ \KwFn{$i+1$, $c''$}\;
	
	\Return $min(c,~c',~c'')$ \;
}

\end{algorithm}

\begin{algorithm}

\caption{Algoritmo que encuentra la distancia mínima entre los acordes de una canción mediante programación dinámica y devuelve el desplazamiento mínimo y la matriz $\Sigma$ con la que se obtienen las escogencias para alcanzar esa solución. El parámetro $m$ se define como una matriz de centroides, de tantas filas como acordes tenga la canción de entrada y de tres columnas, $n$ es la cantidad de acordes de la canción, y la variable $k$ representa el índice del centroide elegido en cada iteración.\label{alg:dinamica}}
\SetAlgoLined

\SetKwFunction{KwFn}{Distancia-Menor}
$(d_{min}, \Sigma[1..n, 1..3]) \leftarrow $\KwFn{$n$,$m[1..n, 1..3]$}{

	\Indp
	$F[n, 1..3] = 0$
	
	\For{$i=n-1,n-2,...,2,1$}{
		\For{$j=1,2,3$}{
			$(min, k)=min\{\text{distancia}(m[i,j], m[i+1,1]+f_{i+1}[0]),\\
             \text{distancia}(m[i,j], m[i+1,1]+f_{i+1}[2]),\\
             \text{distancia}(m[i,j], m[i+1,2]+f_{i+1}[3])\}$
             
		}
	}
	
	$F[i,j]=min$
	
	$\Sigma[i,j]=k $ $//~\text{índice del menor centroide}$

	\Return $(min\{F[1,1], F[1,2], F[1,3]\},\Sigma)$ \;
}

\end{algorithm}



\bibliographystyle{IEEEtran}
\bibliography{Referencias}

\end{document}
